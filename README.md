# Stash Suggest Reviewers plugin

The Stash Suggest Reviewers plugin provides intelligent reviewer suggestions for Stash Pull Requests.

![Pull Requests](https://bitbucket.org/atlassian/stash-suggest-reviewers/raw/170022f40236ce9f94e3c79cad22137787c91fc0/docs/pull-requests.png)

## How it works

Reviewers are suggested by mapping the email addresses of repository contributors to Stash users. The email set in your
[local git config](http://git-scm.com/book/en/Getting-Started-First-Time-Git-Setup#Your-Identity) must match the email
address you have registered in Stash for you to be suggested as a reviewer.

Reviewers that are more likely to be relevant are suggested first. By default, reviewers are suggested using two
different algorithms:

- **Contribution**: authors who contributed commits to the pull request are ranked very highly in the suggestions.
- **Blame**: authors who have previously contributed code to files that are modified by the pull request are also
  suggested. The more they have contributed, the higher their ranking in the suggestions.

The algorithms used to suggest reviewers are a plugin point, so you can add your own reviewer suggestions through a
plugin.

## Adding your own suggestion algorithms

You will need to create a Stash plugin to add your own suggestion algorithms, here's how to do that:

- Install the [Atlassian SDK](https://developer.atlassian.com/display/DOCS/Getting+Started).
- Create a [new Stash plugin](https://developer.atlassian.com/stash/docs/latest/how-tos/creating-a-stash-plugin.html).
- Add a dependency on the Stash Suggest Reviewers plugin to your plugin's ``pom.xml``:

        <dependency>
            <groupId>com.atlassian.stash.plugin</groupId>
            <artifactId>stash-suggest-reviewers</artifactId>
            <version>1.0</version>
        </dependency>

- Create a new class that implements the [ReviewerSuggester](https://bitbucket.org/atlassian/stash-suggest-reviewers/src/master/src/main/java/com/atlassian/stash/suggestreviewers/spi/ReviewerSuggester.java) interface.
- Register the interface by adding the following to your plugin's ``atlassian-plugin.xml``:

		<reviewer-suggester key="a-key-for-your-reviewer-suggester"
                            name="a-name-for-your-reviewers-suggester"
                            class="com.yourcompany.your.SuggesterImplementation" />

Alternatively, you can fork the existing [Stash Suggest Everybody](https://bitbucket.org/atlassian/stash-suggest-everybody-example) 
example plugin that contains a trivial ``ReviewerSuggester`` implementation and make the necessary changes to 
implement your own suggestion algorithm.

More information about Stash plugin development is available in the [Stash Developer Docs](https://developer.atlassian.com/stash/docs/latest/).
