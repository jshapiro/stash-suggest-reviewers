package com.atlassian.stash.suggestreviewers.internal;

import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.history.HistoryService;
import com.atlassian.stash.scm.git.GitScm;
import com.atlassian.stash.scm.git.GitScmConfig;
import com.atlassian.stash.scm.git.merge.GitMergeBaseBuilder;
import com.atlassian.stash.suggestreviewers.internal.util.FirstLineOutputHandler;
import com.atlassian.stash.suggestreviewers.internal.util.GitUtil;

/**
 * Determines the merge base of a pair of commits.
 */
public class MergeBaseResolver {

    private final GitScm gitScm;
    private final GitScmConfig config;
    private final HistoryService historyService;

    public MergeBaseResolver(GitScm gitScm, GitScmConfig config, HistoryService historyService) {
        this.gitScm = gitScm;
        this.config = config;
        this.historyService = historyService;
    }

    public Changeset findMergeBase(Changeset a, Changeset b) {
        GitMergeBaseBuilder builder = gitScm.getCommandBuilderFactory()
                .builder(a.getRepository())
                .mergeBase()
                .between(a.getId(), b.getId());

        GitUtil.setAlternateIfCrossRepository(builder, a.getRepository(), b.getRepository(), config);

        String sha = builder.build(new FirstLineOutputHandler()).call();

        if (sha == null) {
            return null;
        }

        return historyService.getChangeset(a.getRepository(), sha);
    }

}
