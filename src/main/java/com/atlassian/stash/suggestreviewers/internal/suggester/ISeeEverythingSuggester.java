package com.atlassian.stash.suggestreviewers.internal.suggester;

import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.suggestreviewers.spi.Reason;
import com.atlassian.stash.suggestreviewers.spi.ReviewerSuggester;
import com.atlassian.stash.suggestreviewers.spi.SimpleReason;
import com.atlassian.stash.user.StashUser;
import com.atlassian.stash.user.UserService;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

public class ISeeEverythingSuggester implements ReviewerSuggester {

    private static final int SCORE_FOR_TIM = 100000;

    private final UserService userService;

    public ISeeEverythingSuggester(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Multimap<StashUser, Reason> suggestFor(Changeset since, Changeset until) {
        Multimap<StashUser, Reason> suggestions = HashMultimap.create();
        StashUser tim = userService.getUserByName("tim");
        if (tim != null) {
            suggestions.put(tim, new SimpleReason("Mostly due to awesomeness.", SCORE_FOR_TIM));
        }
        return suggestions;
    }

}
